# Frequently Asked Questions

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

![Sampling Party](https://gitlab.com/our-sci/resources/-/raw/master/images/website-images/website%20images/Our%20Projects/Real%20Food%20Campaign/real-food-cheers.png "A circle of hands toasting over a festively-set table")

## What will the program look like this year? 

This season, our Citizen Science program is focused on collecting samples of new and underrepresented fruits and vegetables to better understand crop variability.


Citizen Scientists will be collecting paired produce for comparison--for example, apples from a source you think is nutritious, vs. apples from a source you suspect is less nutritious. We will create a personalized sampling plan and send you all the supplies you need, then you will collect the produce, fill out a survey with details about how and where it was grown, and ship it to our lab in a pre-paid envelope.

## What is the list of sample crops for Citizen Science?

We need your help in building out our data library for the following crops:

- Apple
- Nectarine
- Oats (whole grains)
- Peach
- Pear
- Peppers
- Potato
- Wheat (whole grains)
- Yellow Squash/Zucchini

## What is the time commitment?

That's up to you! Once you complete a sampling plan and receive supplies, the whole process of submitting can be completed in an afternoon: 

* Fill out the Sample Collection survey with information about where you purchased the produce and how it was grown or marketed
* Pack your sample into prelabled return envelopes
* Drop them in the mail
* Congratulate yourself for contributing to scientific research. It's that simple! 

If you have the time and capacity to send a greater number of samples or to organize broader collection efforts, we will support you in that too. This program is entirely dependent on community engagement from citizens who are passionate about food. 











