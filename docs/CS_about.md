
# [APPLY HERE](https://bionutrient.surveystack.io/surveys/60cb65378ee3f10001e20508)

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

![Vegetable Garden](https://upload.wikimedia.org/wikipedia/commons/3/3c/Vegetable_garden_1.JPG "Image of a vibrant vegetable garden")

## About the Program
If you want to become a Citizen Science partner, please fill out the survey above. 

### What is the Citizen Science Partner Program?
The Citizen Science Partner Program is a volunteer project to source produce samples that meet the needs of the Bionutrient Institute labs. This could mean submitting samples of crops that we are lacking in our database, or contributing to specific citizen science projects. 

Past projects have included:

* Monthly sampling: Volunteers collect samples from a diverse population of conventional, organic, and regenerative sources in their area. This is a small time commitment every month for a few months of the year.
* Food Desert Experiment: Volunteers collect samples both in and out of local food deserts to help us better understand the systemic discrepancies in our food system. Where does higher quality, more nutritious produce go, and who has access to it? Do economic and geographic factors limit access to more nutrient dense food? This experiment requires a more structured sampling plan and a larger volume of samples from volunteers. This program is more time-intensive up front, but is only a one-time commitment rather than a monthly task.
* Individualized sampling plans designed with a community question in mind. Contact us! 


### Citizen Scientists commit to...
* Send samples to a BI Lab, following the sampling plan and best practices as closely as possible.
* Be willing to enter a small amount of data into a smartphone app or online survey when the sample is collected.
* Take collected samples to a USPS facility to ship to the lab the same day they are collected.

### The BI commits to...

* Provide Citizen Science Partners with all the sampling equipment they need, including prepaid envelopes and sampling containers.
* Create a reasonable, individualized sampling plan based on feedback for each Data Partner.
* Provide information including documents and videos to communicate with producers about the Bionutrient Institute.
* Provide a simple and low-effort way to give partners access to their data, while keeping their personal information anonymized.
* Follow our Data Privacy guidelines to reassure farmers that their data is secure and appropriately shared.
* To support and engage your personal research interests as much as possible! Contact lab@realfoodcampaign.org if you want to discuss your ideas and how they may fit into the survey or lab process.

## How to become a Citizen Science Partner

* Fill out the interest survey [here](https://bionutrient.surveystack.io/surveys/60cb65378ee3f10001e20508).
* A Bionutrient Institute staff member will contact you to:
    1. Enroll you in the program and help you develop a custom sampling plan.
    2. Conduct training on how to fill out sample surveys and collect food and soil samples.
    3. Provide helpful tutorials and YouTube videos to help explain the process.
    4. Mail you sampling supplies (pre-labelled sample bags, postage paid shipping containers, etc).


Citizen Scientists then submit produce samples and management/labelling information to the BI lab based on their custom sampling plans.
