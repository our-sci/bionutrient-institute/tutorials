# Beef Project Citizen Scientist Collection Kit Instructions

### Overview Checklist  
☐ Freeze cold packs  
☐ Purchase 3 steak samples of the same brand  
☐ Record key data from place of purchase  
☐ Complete Survey: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9)  
☐ Pack and ship the steak samples using the prepaid shipping label

Ensure you have all the materials to ship the steak samples:  
- Cooler box containing:  
    - 1 large cold pack  
    - 8 small cold packs  
    - 1 completed 11-b Priority Mail Express label  
    - 2 gallon zip-top bags  

### Sample Collection Instructions

1. Freeze the cooler packs before packing and shipping.<br>  

2. Purchase 3 ribeye steaks of the **same brand** from a Grocery Store, Farmers Market, Farm Stand, ect. Choose steaks that have a mix of meat and fat. “Steer” away from steaks that are too lean.    
  
    - Make note of how the steaks are advertised: Local (within 100 miles):  
        ☐ Certified Organic  &emsp;
        ☐ Grass-fed  &emsp;
        ☐ Other  &emsp;
    
    - If you are purchasing from a farmer, be sure to ask how the animals were finished (what did they consume during their last 120 days of life):  
        ☐ 100% Grazing  &emsp;
        ☐ Grazing + Feed/Grain  &emsp;
        ☐ 100% Feed/Grains  &emsp;

    - If you are purchasing from a farmer, ask which type of grazing the animal did:  
        ☐ Pasture grazing  &emsp;
        ☐ Rangeland grazing  &emsp;
        ☐ Cover-crop grazing  &emsp;
        ☐ Crop residue grazing  &emsp;  

3. Complete the [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9).  
Take note of the sample number located on the label affixed to the zip-top steak sample bags. You will need this number for the survey and to track your sample data.<br>  
  
4. Packaging and shipping the steak samples:  
    - Affix the 11b form found in the cooler to the outside of the box, covering up the original label. This label is filled out for you.  
    - Place the largest cooler pack (frozen) at the bottom of the box.  
    - Place your steak samples in the gallon zip-top bags labeled “steak” for extra protection against leaks.  
    - Place the smaller cooler packs (frozen) on top of the meat.  
    - Before you seal the box, ensure the Bionutrient Institute survey is complete. 
    - Ship early in the week to avoid any weekend shipping hold-ups.
