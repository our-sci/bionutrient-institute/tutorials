# Introduction to Surveys for Citizen Scientists

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

We use forms in [Surveystack](https://bionutrient.surveystack.io/) to collect information about samples throughout the season. 

## What forms do I need? 
Citizen Scientists need to complete the following forms: 

* [Citizen Science Interest and Onboarding Form](https://bionutrient.surveystack.io/surveys/60cb65378ee3f10001e20508) - This quick form puts you in touch with our team and tells us a little bit about you and your interests. 

* [Sample Collection Survey - Community Partners](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1) - If you're contributing to our regular Citizen Science program by shipping produce samples, this is the form you'll use each time. 


## About Surveys

The information we collect serves the larger goals of the BI:

* Produce an independent, geographically diverse survey of food quality in the US to identify nutrition variation.
* Collect lab measurements and spectral data to calibrate the handheld Bionutrient Meter, which will eventually be used by consumers to predict the nutritional density of food.
* Support BFA members and their research goals.



