# Producers

Since 2018, the Bionutrient Institute has been conducting surveys of farms, markets, and stores across the United States, generating a public database of food quality. We have seen significant variation in nutrient density across crops, as well as a relationship between soil health and nutritional outcomes.

## Submit Samples to the Bionutrient Institute Lab

If you're a farmer, gardener or researcher interested in finding out more about nutrient density in your crops and the soil they're grown in, we'd love to have you participate in the Bionutrient Institute's projects. Our suite of tests and tools will help you understand the physical and biological properties of your samples, and the detailed information you provide gives us crucial context for our research. 

### **Participation**
A linked crop and soil sample costs $195. This includes:

 * Supplies and shipping materials
 * A detailed nutrient report for crop and soil
 * An ongoing FarmOS and Farmer’s CoffeeShop subscription
 * Program training and project support.
 
### **Lab Results**


| Crop Nutrition      | Soil Nutrition |
|  -----------      |  -----------|
| Total Antioxidant content | Total Organic Carbon % |
| Total Polyphenol content     | Soil Respiration      |
| Total protein (for relevant crops)   | Base saturation*       |
| Brix    | Soil Respiration      |
| Total mineral content for: Mg, S, K, Ca, <br> Fe, Zn, P, Si, Al, Mo, Mn, Na, Ni Cl, Cu   | Total and Mehlich III extractable*: S, P, Ca, Mg, K, <br> Na, B, Fe, Mn, Cu, Zn, Al       |
|       | pH |
|        | Total exchange capacity* |


*Only available for samples sent to Ann Arbor laboratory


### **Available Crops**  


![BI Crop Test Visual](https://gitlab.com/our-sci/resources/-/raw/b04ed478375db212ed784e72136ae8481691ad0c/images/Partner%20tutorials/whatcrops.png "Image of 24 fruits, vegetables, and grains available for testing")


### How It Works

 Users can work with our experienced team to develop a crop and sampling plan, then fill out easy-to-use surveys in [Surveystack](https://bionutrient.surveystack.io/). Sampling supplies and pre-labeled envelopes are provided, and results are returned within 3-6 weeks. 


To get started, fill out this survey: **[Lab Services Contact Form.](https://bionutrient.surveystack.io/surveys/626032bb9fab960001e8f532)**


If you have questions, please contact the BI Grower Partner Inquiry email (partners@bionutrient.org). 

