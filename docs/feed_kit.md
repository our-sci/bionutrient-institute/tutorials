# Beef Project Feed Collection Kit Instructions

## Feed Kit Surveys
- [Beef Forage/Feed Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff) -
This survey needs to be completed by the producer when they submit the feed and fecal matter samples. It guides the beef partner through the sampling process and captures key metadata about the fecal and feed samples being submitted.
- [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9) - This survey has two parts:
    - Sample metadata: This part needs to be completed by the producer when they submit steak samples. This form captures key animal metadata like breed, sex, size, etc.
    - Management data: This section is to capture detailed management practices for the last 90-120 days of the animal's life span. This part can be completed by the farmer or, if preferred, project staff can conduct an interview with the farmer to answer these questions over the phone. 

## Overview Checklist  
☐ Freeze cold packs  
☐ Sample & Refrigerate: Feed  
☐ Sample & Freeze: Manure  
☐ Complete Survey: [Beef Forage/Feed Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff)  
☐ Sample & Send: Steak Samples (1-2 months after previous samples), Feed, and Manure  
☐ Complete Survey: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9)

## Step 1: Collect Feed and Fecal Samples
<iframe src="https://player.vimeo.com/video/657546841?h=e7f78faeca" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/657546841">Sample Collection: Fecal</a> from <a href="https://vimeo.com/bionutrient">Bionutrient Food Association</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Collect feed and fecal samples at the same time. Ship early in the week to prevent shipping delays over the weekend.

Ensure you have all the materials to collect feed and fecal samples:  
- 1 prepaid tyvek shipping envelopes with completed 11-b Priority Mail Express label  
- 1 gallon zip-top bag for feed  
- Paper towel   
- 2 small tubes & 2 small spatulas for fecal samples  

When you are ready to collect your feed and fecal samples, begin filling out the 10 minute [Beef Forage/Feed Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff).

1. Collect the grain samples  
        a. Know the feeds/grains you will be using to finish the cattle.  
        b. Collect a handful of feed/grain from 5-7 different parts of the feeding area and place the feed/grain in the gallon zip-top bag.  
        c. Leave air in the zip-top bag and seal.  
        d. Refrigerate the bag before shipping.  
2. Next, prepare to gather the fecal samples. The stool samples should be collected in the morning from fresh cow pies of the finishing herd. If the younger and older animals are staged together, it is important to sample from the larger cow pies as those are more likely from mature animals.  
        a. Use the small spatulas to collect small scoops from 3-4 different cow pies around the pasture. Repeat for the second vial.  
        b. Discard the scoops in the trash.  
        c. Wipe the top of the tubes with the paper towel to remove any fecal matter before screwing the top on.  
        d. Place the tubes back into the snack-size zip-top bag and, if possible, stuff newspaper or alternate padding around the vials as cushion during transit.  
        e. Freeze the fecal samples.  
3. Ensure the Bionutrient Institute Beef Forage/Feed Assessment Survey is complete.   

## Step 2: Collect Steak Samples
Collect steaks approximately 30 days after sending in forage, soil and fecal samples.

Freeze your cold packs before packing.

Ensure you have all the materials to ship the steak samples:
Cooler box containing:  
- 1 large cold pack  
- 8 small cold packs  
- 1 completed 11-b Priority Mail Express label  
- 2 gallon zip-top bags

1. At the time of sampling, fill out [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9).
2. Collect steak samples from **3 separate cows**. These steak samples must come from the same herd that the feed and fecal samples were taken from.  
3. Select ribeye steaks that have a mix of meat and fat. “Steer” away from steaks that are too lean.  
4. Packaging and shipping your steak samples  
    a. Affix the 11-B shipping label found in the cooler to the outside of the box, covering up the original label. This 11-B label is filled out for you.  
    b. Freeze the fecal sample before shipping.  
    c. Place the largest cooler pack (frozen) at the bottom of the box.  
    d. Place your steak, feed, and manure samples into the box.  
    e. Place the smaller cooler packs (frozen) on top of the meat.  
    f. Ship at the beginning of the week so that the samples don’t sit in a shipping facility over the weekend. **SHIP THE SAMPLES USING 1-2 DAY SHIPPING. This is critical to ensure your samples don't arrive rotten.**  
    e. Ensure the Bionutrient Institute [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9) is complete before you seal the box.  
    f. Send the package to:   
```
Stephan van Vliet  
Utah State University  
8700 Old Main Hill  
NDFS Department  
LOGAN, UT, US, 84322  
```
