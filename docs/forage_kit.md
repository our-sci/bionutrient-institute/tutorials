# Beef Project Forage Collection Kit Instructions

## Forage Kit Surveys
- [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff) -
This survey needs to be completed by the producer when they submit the forage/soil or fecal matter samples. It guides the beef partner through the sampling process and captures key metadata about the forage/feed samples being submitted.
- [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9) - This survey has two parts:
    - Sample metadata: This part needs to be completed by the producer or citizen scientist when they submit steak samples. This form captures key animal metadata like breed, sex, size, etc.
    - Management data: This section is to capture detailed management practices for the last 90-120 days of the animal's life span. This part can be completed by the farmer or BI project staff. If preferred by the farmer, project staff can conduct the interview over the phone. 

## Overview Checklist  
☐ Freeze cold packs  
☐ Freeze Forage sample & Ship Soil sample (1-2 months before animals are harvested)  
☐ Complete Survey: [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff) for Forage & Soil Samples  
☐ Sample & Freeze Manure sample (7 days after sampling forage + soil)  
☐ Complete Survey: [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff) for Fecal Samples  
☐ Sample & Send: Steak Samples, Forage Sample and Manure  
☐ Complete Survey: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9)

## Step 1: Collect Forage & Soil  
Video: <iframe src="https://player.vimeo.com/video/657546938?h=1139aa9476" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/657546938">Sample Collection: Forage &amp; Soil</a> from <a href="https://vimeo.com/bionutrient">Bionutrient Food Association</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Collect and ship the soil and forage samples 1-2 months before the animals are harvested. 

Collect and ship the stool samples after the cows have been grazing on the designated pasture for 7 days.  

Ship packages early in the week to prevent shipping delays over weekends.

1. Ensure you have all the materials to collect forage, soil, and fecal samples:  
    - 2 prepaid tyvek shipping envelopes with completed 11-b Priority Mail Express labels  
    - 1 gallon zip-top bag for forage  
    - Paper towel  
    - Something to cut the forage  
    - 2 quart-sized zip-top bags for soil samples  
    - 2 small tubes & 2 small spatulas for fecal samples  
    - Soil probe
    <br />
2. Sample a rested pasture that will be grazed by the harvested animals in the next 30 days. We’re looking for a sample that best represents what the animals will be consuming.  

3. Collect forage and soil samples from 5-7 locations in the pasture.  

4. Beginning in one corner of the pasture, make sure that there is no standing water in the field. Move across the field, sampling in a zigzag pattern. See image below.  

5. At Each Sample Location:  
    a.Cut plant samples from an area of 1 square foot at grazing height, remove any large plant or root material from the samples, pat them dry with the paper towel and add them to the gallon zip-top bag labeled “forage” and then freeze to be shipped with manure and beef samples.  
    b. In the same sampling area, push your soil probe 8 inches (20 cm) into the soil and carefully pull it out. Ensure there is no fecal matter where you are inserting the probe.   
    c. Remove soil from the probe and place into a bag or bucket.  
    d. Repeat these steps at each of the 5-7 sampling locations. Mix soil until combined and then fill the soil sample bag at least half full.  
    e. Mail soil samples to Logan Labs: Logan Labs, 620 N Main St, Lakeview, OH 43331  
    h. Freeze the forage samples and hang onto them as you will be sending this sample in the steak box.  

    Complete the 10 minute [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff) checking the box marked “Forage + soil” on question 7. 

  <img src="https://gitlab.com/our-sci/bionutrient-institute/tutorials/-/raw/master/docs/images/sample_pattern.jpg" alt="alt text" width="300" height="300">


## Step 2: Collect Fecal Samples
Video: <iframe src="https://player.vimeo.com/video/657546841?h=e7f78faeca" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/657546841">Sample Collection: Fecal</a> from <a href="https://vimeo.com/bionutrient">Bionutrient Food Association</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Collect and freeze the stool samples after the cows have been grazing on the designated pasture for 7 days. 
  
6. After the cattle have grazed the sampled pasture for 7 days, prepare to gather the fecal samples. The stool samples should be collected in the morning from fresh cow pies of the finishing herd. If the younger and older animals are staged together, it is important to sample from the larger cow pies as those are more likely from mature animals.  
  a. Use the small spatulas to collect small scoops from 3-4 different cow pies around the pasture. Repeat for the second vial.  
  b. Discard the scoops in the trash.  
  c. Wipe the top of the tubes with the paper towel to remove any fecal matter before screwing the top on.  
  d. Place the tubes back into the snack-size zip-top bag and, if available, stuff newspaper or alternate padding around the vials as cushion during transit.  
  e. Freeze the fecal samples and hang onto them as you will be sending this sample in the steak box.  

9. Complete the 10 minute [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff), checking the box marked “Fecal” on question 7.  

## Step 3: Collect Steak Samples
Collect steaks 1-2 months after sending in forage, soil and fecal samples.

Freeze the cold packs before packing.

Ensure you have all the materials to ship the steak samples:
Cooler box containing:  
  - 1 large cold pack  
  - 8 small cold packs  
  - 1 completed 11-b Priority Mail Express label  
  - 2 gallon zip-top bags

10. Collect steak samples from 3 separate cows. These steak samples must come from the same herd that the forage/soil, feed and fecal samples were taken from.  

11. Select ribeye steaks that have a mix of meat and fat. “Steer” away from steaks that are too lean.  

12. Packaging and shipping your steak samples
  a. Affix the 11b form found in the cooler to the outside of the box, covering up the original label. This label is filled out for you.  
  b. Place the largest cooler pack (frozen) at the bottom of the box.  
  c. Place your steak samples in the gallon zip-top bags labeled “steaks” for extra protection against leaks.  
  d. Place the smaller cooler packs (frozen) on top of the meat.  
  e. Ensure the Bionutrient Institute survey is complete before you seal the box. Take note of your sample number on the label on the sample bags – you will need this number to track your sample.  
  f. Ship samples at the beginning of the week so that the samples don’t sit in a shipping facility over the weekend.  
  g. Ship the Forage, Steak, and Manure samples to:  
    _Stephan van Vliet  
    Utah State University  
    8700 Old Main Hill  
    NDFS Department  
    LOGAN, UT, US, 84322_  
    
13. Complete the [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9).
