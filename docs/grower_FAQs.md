# Frequently Asked Questions

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

## Sampling

### *How many produce samples can I submit?*

For the 2021 season, we are initially looking for three samples from each partner. As the season continues and we have additional processing capacity or new data needs, we may reach out about submitting additional samples. In addition, you are welcome to submit as many additional samples as you like at cost: a suite of three crop samples and six soil samples can be run for $200, or $450 for a suite of nine.

### *Are my sample collection materials from last year still valid?*

Tools like your soil corer are still functional for this season, but since we are consolidating samples from three bags down to one, we will send you new sampling bags that correspond with your new plan. We are also using QR codes instead of barcodes for our labels for easier scanning. This should make the sample collection process simpler for everyone involved, but since we have changed our protocol for 2021 sample collection, please do not send in any of your samples in last year’s bags.

## Surveys 

### *What surveys do I need to fill out?*

-  **Grower Partner Interest and Onboarding Form**: This is the first step, to tell us a little bit about your operation and contact information.

- **Planting Form** - Complete this after you’ve done initial treatments for the fields/beds you plan to sample. This survey gives us the information we need to connect management practices to nutrition down the line. 

- **Sample Collection Survey** - Complete at the same time that you collect and ship your samples - the information in this survey is crucial for ensuring quality data in the lab. 

- **Harvest Survey** - Complete this after the final harvest of your sample crop or the end of your season, whichever is first. We have datasheets available to simplify the process of recording this information throughout your busy season. 

### Filling Out A Survey

<iframe class="embedded-content" src="https://our-sci.gitlab.io/software/surveystack_tutorials/filling_out_surveys/"></iframe>

<script>
document.querySelector('.embedded-content').addEventListener("load", ev => {
    const new_style_element = document.createElement("style");
    new_style_element.textContent = ".wy-nav-top { display: none;} footer { display: none;} ul.wy-breadcrumbs {display: none;}"
    ev.target.contentDocument.head.appendChild(new_style_element);
});
</script>

### *How will the SurveyStack forms used this year differ from the ones used last year?*

We have updated our forms to be more user-friendly and functional, with streamlined question pages and a new matrix feature to cover multiple treatments. If you have suggestions for how to improve your user experience, please reach out to us.

## FarmOS

### *What is FarmOS?*

We use FarmOS as a tool to log information within our SurveyStack forms, and it's also a powerful way to manage farm information on its own. To find out more about the work they are doing, visit them here: [FarmOS.org](https://farmos.org/)

### *How do I make a new field in a Planting survey?*

We're excited to bring you new functionality to draw your FarmOS fields directly in our Planting form. Here is a step-by-step guide. 

When you reach the first question in the Planting form, you can either select a field you already have in your FarmOS account from the dropdown list, or click NEXT to create a new field. 

![Field](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/field.png)

On the next screen, you'll be able to name and nickname your new field, then click the target icon to zoom in to your location. 

![world map](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/targetlocation.png)

This function will narrow in on wherever your device is located. You can click and drag the map, or use the plus and minus icons to navigate to your field location. 

![zoom](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/zoom.png)

Alternately, if you are inputting a field location that isn't where you are currently located, you can click the magnifying glass icon and type in an address. 

![search](https://gitlab.com/our-sci/resources/-/raw/6f887ac772c83032ab12fc420b2e14a92d741e62/images/Partner%20tutorials/farmos/address.png)

Once you're in the right spot on the map, click the square icon on the left-hand side (highlighted in yellow) to draw your field. Knowing your precise field dimentions will help you get the most accurate polygon, but as long as it's approximately the right size and the right place and you know which fields are which, we'll be able to record all the information we need. 

![draw](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/draw.png)

If you've drawn your polygon and need to adjust it, click the square with points icon (highlighted in yellow) and then drag each corner of your polygon to the right place. 

![adjust](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/edit.png) 

If your polygon is the right size and shape but you need to move it to a different location, click the navigation icon (highlighted in yellow) and drag your polygon. 

![move](https://gitlab.com/our-sci/resources/-/raw/6091acbf3f70210e6160ea4eb0f77a85ab3d20bf/images/Partner%20tutorials/farmos/move.png)

When a polygon is selected, it will be opaque and you'll see the option to delete the polygon using the trash bin icon. It can help to draw your shape a few times to make sure you're recording the most accurate and useful field dimentions. 

![delete](https://gitlab.com/our-sci/resources/-/raw/0e89f8273d4005f93a9547ef664ece96042c5bec/images/Partner%20tutorials/farmos/delete2.png)

Once you're satisfied with your field, hit NEXT, and the information you enter will be linked with this FarmOS record when the survey is submitted. 

## Data

### *When can I expect to get my results back?*
This year, our anticipated data turnaround is six weeks, but you may receive your results in as little as three. 
 

### *How will this year’s results differ from last years?*

That’s part of what we’re excited to find out! As we build our library of nutrition information, we’ll be able to ask more and better questions about management results and best practices. If you’re interested in conducting your own experiment, check out our Experiments page. 
