# Introduction to Surveys - Grower Partners

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

We use forms in [Surveystack](https://bionutrient.surveystack.io/) to collect information about samples throughout the season. 

## What forms do I need? 
 - **[Grower Partner Interest and Onboarding Form (2021)](https://bionutrient.surveystack.io/surveys/608ac270ab78b1000102b55f)** - This was the first step, to tell us a little bit about your operation and contact information. **Note: If you are currently interested in contacting us about submitting samples, please fill out the [Lab Services Contact Form](https://bionutrient.surveystack.io/surveys/626032bb9fab960001e8f532) instead.**
 - **[Planting Form](https://bionutrient.surveystack.io/surveys/611aceeb7a10b50001a86ce3)** - Complete this after you’ve done initial treatments for the fields/beds you plan to sample. This survey gives us the information we need to connect management practices to nutrition down the line. 
 - **[Sample Collection Survey](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1)** - Complete at the same time that you collect and ship your samples - the information in this survey is crucial for ensuring quality data in the lab
 - **[Harvest Survey](https://bionutrient.surveystack.io/surveys/6182f272ba958a0001f240d7)** - Complete this after the final harvest of your sample crop or the end of your season, whichever is first. We have [datasheets](https://docs.google.com/spreadsheets/d/15L_VkBqGpSd9KeiDih2PQ27Ls7GSbBA7cSNis89uPdA/edit#gid=0) and [instructions](https://docs.google.com/document/d/1kRYhFzz_WN_fJvvTCBR63VbY2jiqvbOg/edit) available to simplify the process of recording this information throughout your busy season. 

## Registering an Account to use Group Surveys

Please follow these steps to create a SurveyStack account. This is where you will fill out surveys for the samples you send in.

1. You will receive an email from invitation@surveystack.io with the subject line “Surveystack invitation to group Partners”. Click the link in the email to activate your invitation.

![Invitation](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/invite1.png)

Note: If you are affiliated with a program such as NOFA or PASA, that is the group name that will be on your invitation, e.g. “Surveystack invitation to group PASA”. 

2\. This link will take you to the invitation page, where you should click `Create account` and fill in your information to sign up.


![Register](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/register.png)

3\. After creating your account, click `join Partners` and you will be taken to the home screen.

![Invitation 2](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/invitation.png)

4\. At the home screen, click `Browse All Surveys`. From there, you’ll be able to see all the surveys available to your group and submit information. 

![Logged in](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/loggedin.png)


## Filling Out a Form


<iframe height="500" class="embedded-content" src="https://our-sci.gitlab.io/software/surveystack_tutorials/filling_out_surveys/"></iframe>
 
Here is a [video tutorial](https://www.youtube.com/watch?v=j2Gzjid4o2I) walking through the steps of filling out a previous year's planting form. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/j2Gzjid4o2I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Sample Submission Timeline
* *May*
    
    - Interest and Onboarding form
    - <p>Planting form for Kale

* *June*

    - <p>Planting form for Peppers 
    - <p>Planting form for Zucchini

* *July*

    - <p>Sample Collection form for Kale
 
* *August*

    - <p>Sample Collection form for Zucchini
    - <p> Sample Collection form for Peppers
    
* *September*
     - <p>Harvest forms for Kale, Peppers, and Zucchini



## End-of-Season Checklist

 *Have you submitted...*

 -  ☐  Interest and Onboarding form 
 -  ☐ Planting form for Crop 1
 -  ☐ Planting form for Crop 2
 -  ☐ Planting form for Crop 3
 -  ☐ Sample Collection form for Crop 1
  >> ☐ *Mailed Sample* 
 -  ☐ Sample Collection form for Crop 
  >> ☐ *Mailed Sample* 
 - ☐ Sample Collection form for Crop 3
  >> ☐ *Mailed Sample* 
 - ☐ 	Harvest Form for all crops


 