# Bionutrient Institute: International Beef Project Collection Kit Instructions

## Farmer Overview Checklist
☐ Order shipping box with a styrofoam cooler insert with **inside dimensions** that are approximately 30.5 cm wide x 25.4 cm long x 18 cm tall (12 inches x 10 inches x 7 inches). [See example](https://www.uline.com/Product/Detail/S-15181/Insulated-Shippers-and-Supplies/Insulated-Foam-Shipping-Kit-12-x-10-x-7)  
☐ Order 4.5 - 5.5 kg (10-12 pounds) of dry ice  
&emsp;&emsp;*NOTE: You will need to have the cooler box, stool samples, meat samples and the dry ice before packing. Because dry ice is the most sensitive part of the shipping package, order your dry ice after you know when you will have your cooler box, stool samples, and meat samples ready to go.  
☐ Complete Survey: [Beef Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff)  
☐ Sample & ship your Forage/Feed in Box #1  
☐ Sample & ship your Soil (if sampling forage) in Box #2
☐ Sample the Feces - the feces samples should come from the same herd as the meat samples. Freeze the samples  
☐ Sample 3 of your farm’s Steak Samples, freeze the samples  
☐ Purchase 3 grocery store ribeye steaks as control samples for your region (all three coming from the same brand / company), freeze the samples  
☐ Complete Survey: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9) for your farm steaks  
☐ Complete Survey: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9) for the store bought steaks  
☐ Package the Feces and Steak samples and ship to US with appropriate labels and documentation in Box #3   

## Step 1: Forage Sample (Box 1)

##### SURVEYS VIA SURVEYSTACK:  

Complete the [Forage Assessment Survey](https://bionutrient.surveystack.io/surveys/6179b8c2ddc4560001060cff). You may do this before you receive your sampling kit. This survey contains questions about the field that the cows will be grazing for the last 30 days of life so it is helpful to complete this survey in the field on an electronic device that has an internet connection.   

##### PACKAGE:  

- You will send your forage samples separately from all your other samples 60-30 days before the animals are slaughtered. Please collect soil at the SAME TIME as you collect your forage!  
- If samples are wet, either fresh or fermented forage, they should be put in vacuum-sealed bags to eliminate oxygen, and also would be recommended to be kept cold. Especially the “fresh” (not fermented), wet samples, which you may want to consider freezing, then shipping with some sort of cold packs. If samples are dry (<15% moisture), then no need to follow above steps.  
- Dairyland's FDA permit covers most feed types. To minimize issues with getting samples through customs, please avoid shipping whole/intact seeds and samples that contain animal byproducts.  
- Write the sample ID number on the bag with a permanent marker. Sample IDs start with the letter “B”.  
- Double-bag the sample to prevent spills during transit. Pack the box with newspapers to keep the sample tight and to avoid shifting during shipment.  
- Print the **Dairyland Sample Submission Worksheet** (found in email from staff), fill it out with your name, address, sample ID number and the species composition and place it inside the box.  
- Print Dairyland's FDA permit to be included in the package samples are shipped with.  A copy of this permit can be downloaded here: [FDA permit for foreign feed samples](https://www.dairylandlabs.com/media-library/documents/5-19-22-10-52_foreignsamplepermitexpiresapril23.pdf)  
- Ship samples via FedEx to: **Dairyland Labs 217 E. Main St. Arcadia, WI 54612**  
  
Please let us know if you do not plan on sending us a forage sample. If you’re collecting grain please let us know and we will send special instructions.


## Step 2: Soil Samples (Box 2)
  
You will collect soil at the same time as you collect forage but you will send your soil samples separately from all your other samples. If you are collecting grain samples you will not collect soil samples.  
  
1. Soil kit: You will have received 1 quart sized zip-top bag labeled “Soil 0-8 inches (0-20cm)” + a Permit, Sample Submission Worksheet, and PPQ Form 550 via email. You will need a soil probe about 1 inch in diameter and at least 8 inches long (not included).  
    a. Collect the soil samples from 5-7 locations in the pasture:  
    b. Make sure that there is no standing water in the field and begin sampling in one corner of the pasture. Move across the field, sampling in a zigzag pattern. See image.    <img src="https://gitlab.com/our-sci/bionutrient-institute/tutorials/-/raw/master/docs/images/sample_pattern.jpg" alt="alt text" width="300" height="300">  
    c. Push your soil probe 8 inches (20 cm) into the soil and carefully pull it out. Ensure there is no fecal matter where you are inserting the probe.  
    d. Place the 8 inches (0-20 cm) of soil from the probe into the bag. All of the soil will be combined in this bag.   
    f. Repeat these steps at each 5-7 sampling locations.  
    g. When finished, remove all of the air from the soil sample bags and seal them.  
2. Print the _Permit, Sample Submission Worksheet, and PPQ Form 550_ that were emailed to you. Attach the PPQ Form 550 to outside of the box, place the Permit and Submission Worksheet inside the box.  
3. Ship your soil samples **via FedEx** to:  
**Logan Labs  
620 North Main St.  
Lakeview, OH 43331**  

## Step 3: Fecal Samples (Box 3)  
<iframe src="https://player.vimeo.com/video/657546841?h=e7f78faeca" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/657546841">Sample Collection: Fecal</a> from <a href="https://vimeo.com/bionutrient">Bionutrient Food Association</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
1. Ensure you have received the following supplies in the mail from the US:  
    - 2 fecal sample tubes with liquid preservative (tris-EDTA)  
    - 2 small spatulas to scoop the fecal matter into the tube  
    - A USDA International Shipping Permit  
    - A Letter to the USDA to include in your kit  
    - A Customs Invoice form  
2. Collect the fecal samples from the herd before the animals are sent to slaughter.  
3. Avoid skin contact with the preservative (tris-EDTA) in the collection tubes. See the MSDS sheet for the tris-EDTA with the kit materials for more information.  
4. The stool samples should be collected in the morning from fresh cow pies of the finishing herd. If the younger and older animals are staged together, it is important to sample from the larger cow pies as those are more likely from mature animals.  
    a. Use the small spatulas to collect small scoops from 3-4 different cow pies around the pasture. Repeat for the second vial.  
    b. Discard the scoops in the trash.  
    c. Wipe the top of the tubes with the paper towel to remove any fecal matter before screwing the top on.  
    d. Place the tubes back into the snack-size zip-top bag and put them in the freezer.  
    e. Take note of your sample number on the label of the plastic bag – you will need this number to track your sample.

## Step 4: Meat Samples (Box 3)  
1. Overall, you will be sending 6 steaks: **3 from your farm** and **3 from a grocery store**.  
2. Ensure you have purchased the appropriate materials:  
    a. Insulated shipping box (cardboard box with styrofoam insert): **inside dimensions** should be approximately 30.5 cm wide x 25.4 cm long x 18 cm tall (12 inches x 10 inches x 7 inches). [See example](https://www.uline.com/Product/Detail/S-15181/Insulated-Shippers-and-Supplies/Insulated-Foam-Shipping-Kit-12-x-10-x-7)  
    b. 4.5 - 5.5 kg (10-12 pounds) of dry ice  
3. Ensure you have accounted for the following items that were shipped to you from the Bionutrient Institute. You will need them for sending your package:  
    a. Customs Invoice Form (2)  
    b. Letter to USDA (2)  
    c. USDA International Shipping Permit (2)  
    d. Stickers:  
        &emsp;&emsp;i. *Dry Ice* Symbol sticker  
        &emsp;&emsp;ii. "Not for Human Consumption"  
        &emsp;&emsp;iii. "Exempt Animal Specimen"  
4. Collect the **farm** steak samples from **3 separate cows**. These steak samples must come from the same herd that the fecal samples were collected from.  
&emsp;- Collect ribeye steaks that have a mix of meat and fat. Steer away from steaks that are too lean.   
5. Purchase 3 ribeye steaks from a **grocery store** as the control samples for your region. Look for feed-lot or grain-fed steaks. If those aren’t available, look for steaks that don’t have any certifications on the label. You will enter information from the label into the Beef Collection Survey.  
&emsp;- Ensure all 3 steaks are from the **same** retailer/farm. Do not buy different brands.  
6. Place the steaks inside the gallon zip-top bags labeled with your assigned **store** sample number. Double check that your sample numbers are correct via the email from BI staff. IF YOUR SAMPLE NUMBERS DON'T CORRESPOND WITH THE CORRECT STEAKS, YOUR DATA WILL BE BACKWARDS.  
7. Freeze all steaks before packing the shipping box.
8. Complete the 10 minute Bionutrient Institute: [Beef Sample Collection Survey](https://bionutrient.surveystack.io/surveys/6149d70a2861780001bb00a9)  **once for your farm steaks and once for your conventional, store bought steaks** before packing and shipping. Ensure there is a survey complete for both of your assigned sample numbers and they correspond to the appropriate sample numbers.  

## Step 5: Ship
☐ The forage samples should be shipped separately. See “forage” section, above.  
☐ The soil samples should be shipped separately. See “soil” section, above.  
☐ The meat and fecal samples should be frozen before packing your cooler box.  
    **ONLY SHIP ON A MONDAY OR TUESDAY AND ONLY USE FEDEX.**  
☐ A week before planning to ship, please contact PARTNERS@BIONUTRIENT.ORG to notify us that you're planning to ship. This allows time for our research team to contact the broker who will guide the package into the US.  
☐ You will need to have the meat samples, stool samples, cooler box and the dry ice before packing and shipping. Pack your cooler box on the day you receive the dry ice (plan for the dry ice to arrive after you know you’ll have the stool, meat, and shipping box).  
☐ Place the dry ice at the bottom of the cooler and the meat and fecal samples on top.  
&emsp;- Ensure the steak sample bags have a “Not for Human Consumption” label affixed to the outside of each bag. If they don't come pre-labeled, please label them before packing.  
☐ Place the styrofoam lid securely onto the top of the cooler. Don't tape the box shut yet.  
☐ Take the box to a shipping service (preferably FedEx if available in your area). We will try to include a sticky plastic document sleeve for the outside of the shipping box, but if your kit does not come with one, the shipping company will be able to provide one.  
☐ Place inside the box (on top of the cooler lid):  
&emsp;- Letter to the USDA  
&emsp;- International Shipping Permit  
&emsp;- A completed Customs Invoice Form  
&emsp;- The MSDS print out for the tris-EDTA that is in the fecal tubes  
☐ Place inside a plastic shipping sleeve on the cardboard outside of the box:  
&emsp;- The “Dry Ice” symbol sticker and the “Exempt Animal Specimen” sticker, and if applicable, the "Keep Refrigerated" stickers  
&emsp;- The USDA International Shipping Permit  
&emsp;- A copy of the completed Customs Invoice Form (ensure every field is complete on this form). Share a copy of this form with PARTNERS@BIONUTRIENT.ORG  

☐ Address the shipping label to:  
```
Stephan van Vliet  
Utah State University  
8700 Old Main Hill  
NDFS Department  
LOGAN, UT, US, 84322  
1 (217) 778-5001  
```


☐ Tell the FedEx clerk who is creating the shipping label that the package has a broker. The broker information to include on the label is:  
```
Alex Crandall
Customs Brokerage Agent
Expeditors Int'l of WA Inc
350 North John Glenn Road
Building 2, Suite 200
Salt Lake City, UT 84116
Direct: +1 (801) 303-6473  |  Office: +1 (801) 303-6400
```

☐ Pay for shipping and send! **Have the FedEx clerk double check your work** on all the forms. Do not neglect to complete "gross" (package) and "net" (meat sample) weight.  

☐ After shipping, please share the tracking code and signed Customs Invoice Form with PARTNERS@BIONUTRIENT.ORG the same day.  
**THIS MUST BE DONE SO THAT YOUR SHIPMENT DOES NOT GET HELD UP IN CUSTOMS.**  
