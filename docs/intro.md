# Introduction to the Beef Project 

Hello! Welcome to the Bionutrient Institute Beef Project. In this section, you will find instructions on how to collect your Beef Project samples.

If you have questions, please contact the BI Grower Partner Inquiry email (partners@bionutrient.org). 

We hope that this project will help you understand the links between soil quality, forage/feed quality and meat quality in your operation. **The best way to do that is to take the appropriate samples at the appropriate times.**

On each page is a general checklist overview of sampling order depending on your sample plan. Please refer to your respective kit instructions before sampling or sending. **Make note of your sample number before you ship.** This number is printed on a sticker located on the sampling bags.

NOTE: For shipping domestic, the Priority Mail Express packages need to be either dropped off to a USPS clerk, picked up using the scheduled pick up service, or dropped into a Priority Mail Express Blue Box. Do not drop these packages into a normal, non-priority, blue mailing box - there is no guarantee that they will arrive in less than two days if they are not shipped properly.

## **For sampling instructions, please select your kit from the menu on the left underneath "Beef Grower Partners"**
