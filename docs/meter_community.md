## Getting Started
You must have a Bionutrient Meter in order to participate in the Meter Community program. If you don't have a meter but would still like to contribute to our data collection work, see the other Citizen Science program in the menu, or click [here](https://our-sci.gitlab.io/bionutrient-institute/tutorials/CS_about/).

If you haven't already, please [fill out this survey to get your onboarding process underway](https://bionutrient.surveystack.io/surveys/626032bb9fab960001e8f532).  This is where we will begin designing a personalized plan that works for you. Once we have your survey, a BI staff member will reach out to create a customized sampling plan.  

## Collecting and Sending Samples  
**Step 1** - You will receive sampling kits in the mail based on your sampling plan. Each sampling kit consists of:  

- A bubble wrap mailer  
- Two sampling bags with individual sample number labels  
- Sampling instruction sheet  
- A prepaid shipping label  

**Step 2** - Purchase your samples:  
For each crop you choose to sample, you will sample two individuals in an apples-to-apples style comparison: one from a store where you purchase your own fruits and vegetables, along with an identical sample from somewhere you would be very unlikely to purchase your fruits and vegetables from. In other words, somewhere you'd suspect the quality of the produce to be poor and somewhere you'd expect the quality to be high.  

Below is the list of produce available to sample as part of this program:


- Beet

- Bok Choy

- Carrot

- Kale

- Lettuce

- Mustard Greens

- Oats (whole grains)

- Swiss Chard

- Yellow Squash/Zucchini

- Wheat (whole grains)


**Step 3** -  Prepare your Bionutrient Meter. Make sure your device is charged and calibrated.   

**Step 4** - Lay out all your sampling supplies and pull up the [Nutrient Density Estimate Survey](https://bionutrient.surveystack.io/surveys/60d38c7f6bc1da00013e9ba2) on your phone or computer.  

**Step 5** - Organize your samples so they correspond to the appropriate kits & sample numbers before you begin. This will ensure you are assigning the correct sample number to the correct sample in the survey. It is critical that the sample number is the same as what you entered into the survey!  

**Step 6** -  Follow the instructions in the Nutrient Density and Sampling Survey to collect the data. Repeat this survey for each piece of produce. For example, if you are sampling carrots, you will place each carrot sample (one from your "good" source and one from your "poor" source) into different zip-top bags with different sample numbers, however, the contrasting samples will go into the same bubble mailer envelope or kit. These are your contrasting samples (or apples-to-apples samples) and it is important you keep them in the same kit so we know that they are the ones you want to compare.  

**Step 7** - After you have completed the survey for your sample, place the sample in the zip-top bag and seal it, leaving some air in the bag. This will help preserve the sample during transport.  

**Step 8** - Pack your kit (remember: both your contrasting produce samples go in separate sample bags, but in the same bubble mailer). Make sure the prepaid label is securely attached, and ship.  
Plan to mail them early in the week so the samples are sure to arrive quickly, and ship your samples as soon as possible after you've purchased them so that they're less likely to spoil.  

**Step 9** - Congratulate yourself on contributing to our Bionutrient Meter models! We couldn't do it without you.
