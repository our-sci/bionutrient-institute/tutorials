# About the Meter

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

The Bionutrient Meter is an open source, hand held spectrometer that has been calibrated with the crop samples collected by the Bionutrient Institute Lab over the past three years. 

![Meter drawing](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/4213e7cd-2dfd-5cfb-e241-307370e700c9.png)



We have currently developed models for 10 crops to be used with the meter. 

|   |  |
|  -----------      |  -----------|
| Beets        | Mustard  |
| Bok Choy        | Oats |
| Carrots        | Swiss Chard   |
| Kale        | Wheat  |
| Lettuce        | Zucchini  |


These models are still in development, and you can playing a critical role by helping to improve them with your data.

![Meter results](https://gitlab.com/our-sci/resources/-/raw/master/images/Blog%20Post%20Images/Data%20Point/prediction%20(1).png)
For more on how the meter works [click here](https://our-sci.gitlab.io/manufacturing/reflectometer-tutorials/).  

## The Meter Community Program

The Bionutrient Institute is working to analyze samples from a wide range of farm practices, environments, and geographic areas, building a rich series of overlapping datasets which can help us understand the connections between management, environment, soil health and nutrient density. This is where you come in!

If you have a meter or plan on purchasing one soon, we need your help to contribute data to further validate our models. As a part of the Meter Community, we know you're engaged and connected with your local food community, and are poised to make observations about quality. 

 This citizen science program involves purchasing farmstand or grocery store samples, testing them with your meter and answering a few questions about where they came from, then sending those samples into our lab for analysis. We will use your meter data and compare it to the lab data taken from the same pieces of produce to validate and further improve the BI Meter readings. Through participation in this program, you will receive advanced data back-- including additional mineral information not provided by the meter. You'll also be ground-truthing what the meter is capable of, and adding a far greater richness and variety to our sample set than we could ever accomplish on our own. 

 To participate, please contact victoria@bionutrient.org with a list of which crops you are interested in submitting. Thank you!  

