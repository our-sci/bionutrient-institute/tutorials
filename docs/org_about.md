<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

# Organizational Partners Program

## What is an Organizational Partner?

Organizational partners are a diverse group, with individual needs and uses for the tools we provide. Here are some of the ways that partners can engage:

- **Compare a farm, network, or community against our database of nutrient density outcomes:** Since 2018, the Real Food Campaign (now the Bionutrient Institute) has been building a data-rich public library connecting thousands of nutrient density outcomes to soil nutrition and farm practices. It currently covers 21 crops collected from a diverse array of stores, farmers markets, and farms across the USA and Europe. Your participation includes access to this library, and helps us continue to update it.

- **Improve nutrient transparency in a farm or supply chain:** The BI provides detailed reports on more than a dozen nutrient density and soil nutrition outcomes, allowing you to pinpoint the measurements that matter to you.

- **Connect farming practices to nutrition:** The BI has developed custom, continually updated data collection tools that help farmers log their management practices and observe which practices lead to increased nutrition outcomes.

- **Create a new study:** BI staff are experienced in designing nutrient density experiments and can help you create a custom experiment that addresses your community's questions.

Depending on your community and desired outcomes, the BI can help you develop a custom program using our tools and experience. Included in this support are online tutorials and connection with our experienced staff who can help your organization succeed. 

**If you have a vision for how we can collaborate in one of these areas, or an idea for using our tools and systems to support a project you've already developed, please reach out to us at partners@bionutrient.org** 


## How can the Bionutrient Institute Lab help your organization?

### The BI’s software and data collection tools simplify farmer participatory research. 
* Invite and manage a network of farm partners
* Use the BI’s existing farm management and sample collection surveys to start collecting data right away
* Admin tools allow project managers to quality-check data as it comes in
* Automated reporting of soil and nutritional data

### Submit crop and soil samples to a single location, and receive back an extensive suite of results, linked and trackable through time and space


| Crop Nutrition      | Soil Nutrition |
|  -----------      |  -----------|
| Total Antioxidant content | Total Organic Carbon % |
| Total Polyphenol content     | pH      |
| Total protein (for relevant crops)   | Base saturation*       |
| Brix    | Soil Respiration      |
| Total mineral content for: Mg, S, K, Ca, <br> Fe, Zn, P, Si, Al, Mo, Mn, Na, Ni Cl, Cu   | Total and Mehlich III extractable*: S, P, Ca, Mg, K, <br> Na, B, Fe, Mn, Cu, Zn, Al       |
|        | Total exchange capacity* |


*Only available for samples sent to Ann Arbor laboratory

### Compare and your outcomes to our multi-year database with comprehensive visual tools

Benchmark your network's results against the BI’s data-rich library of nutrient density outcomes related to soil nutrition and farm practices. This library currently contains thousands of crop and soil samples from 24 crops collected from a wide array of stores, farmers markets, and farms with diverse management practices across the USA and Europe. 

**What crops can I test?**

![BI Crop Test Visual](https://gitlab.com/our-sci/resources/-/raw/b04ed478375db212ed784e72136ae8481691ad0c/images/Partner%20tutorials/whatcrops.png "Image of 24 fruits, vegetables, and grains available for testing")
*What if my crops are not on the list?*

We can work with your team or independently to conduct a literature review and identify requirements for analysis, and develop methods for lab intake, dilutions, and model building. Contact us for more details. 


### Discuss results within your research group, or with other groups and users via the Farmers CoffeeShop. 

Your data will be available in the Farmer’s CoffeeShop (Coming April 2022!), a visual benchmarking and social networking tool designed to support strategic decision-making on farms, encourage knowledge sharing, and improve the information cycle. By sharing outcomes and practices in conversation with other producers, users gain insight based on the variables that matter to their operation holistically, rather than through decontextualized data points. 

## How the Organizational Partner Program works: 
Depending on your community and desired outcomes, the BI can help you develop a custom program using our tools and experience. Included in this support are online tutorials and connection with our experienced staff who can help your organization succeed.

### Project management support

- Use SurveyStack to invite and manage a network of farm partners
- The BI’s existing field management and sample collection surveys allow you to start collecting data right away - or quickly design new surveys using our Question Set Library
- Admin tools allow project managers to quality-check data as it comes in, and automate reporting of soil and nutritional data 

### Prices

Single samples - including supplies and shipping materials, a detailed nutrient density report benchmarked against the BI database, program training and project management support–  start at $145 for a crop sample, or $195 for a linked crop and soil sample, which includes a subscription to FarmOS.

If you have a vision for collaboration in one of these areas, or an idea for using our tools and systems to support a project you've already developed, please reach out. 
Email partners@bionutrient.org to schedule an informational call and we’ll work together to develop your partnership plan. Or post your question on [our community forum!](http://forum.goatech.org/c/bionutrient-institute/9)
