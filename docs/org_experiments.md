# Designing an Experiment

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

![Desk Science](https://gitlab.com/our-sci/resources/-/raw/master/images/website-images/website%20images/desk-science.jpg)

Since 2018, the Bionutrient Institute (formerly the Real Food Campaign) has been working to develop a data-rich library of nutrient density outcomes related to soil nutrition and farm practices. This library already covers 21 crops collected from a wide array of stores, farmers markets, and farms with diverse management practices across the USA and Europe. BI staff are experienced in designing nutrient density experiments and can assist your community in developing a sample plan that addresses your questions. Here’s how you can use our data: 

1. If you’re interested in answering a question based on data we already collect, such as comparing nutrient density between certain varieties, it’s all available--the only limit is your capacity for inquiry. Most of your complex questions can probably be asked and answered within our existing format! We are also able to add or alter the specific management questions in our forms to capture the information that's most valuable to you. 

2. Using the “Additional notes” field in our surveys, you can record information about your own samples, or collaborate with others to all submit similar information in this field, which will then become part of the data library. 

3. If you have a more complex question involving advanced functionality within our surveys, send us a request to join our “Experiments” sub-group and we’ll work with you to include the information you need in future surveys.


