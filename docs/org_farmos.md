# Managing Partners Through SurveyStack

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

While most of SurveyStack is useable without admin access, you or someone on your team may request access for managing partners, sending mass emails, or other admin functions. 

In your group’s Surveystack page (likely [bionutrient.surveystack.io](https://bionutrient.surveystack.io/0) or a URL unique to your project), click the menu button in the top left corner and select `Groups`.

![Surveys Tutorial 1](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys1.png "description")

Here, select the `Partners` subgroup. 

![Surveys Tutorial 2](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys3.png)

Then select the subgroup with which you are affiliated. 

![Surveys Tutorial 3](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys4.png)

In the top right corner, select the `Admin` option. 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys5.png)


Here, you have options to pin surveys, invite members, and otherwise organize contributions to your group. 

## Inviting Members to your Group 

Scroll down to the `Members` section of the admin page, and select the blue button that says `New...` in the right-hand corner. 

![Invitations page](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/invite.png "Invitations page")

In this page, you can enter the emails of users, and choose to send them an invitation to your group. 

## Call for Submissions

In the `admin` page, you'll see a button in the top right corner that says `Call for Submissions'. 

![Call for Submissions page](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/callforsubmissions.png)

Here you can send out mass survey invitations to all members whom you have previously invited to join your group. 

## Onboarding with FarmOS 
As of this year, you can onboard farms to FarmOS and send invitations directly from your Surveystack group. Here’s how:

In the Admin page, scroll down to the Farm Hub Onboarding section and select `Manage FarmOS Farms`


![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys6.png)

In this frame, do NOT select New Aggregator. Instead, click the arrow to expand SurveyStack. 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys7.png)

Then select `New Farm`. 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys8.png)

Here is where you can input the information for your new farm partner. 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys9.png)

*Note*: There is a character limit for these boxes, so if you receive an unspecified error message, you may need to shorten or abbreviate the full name of a farm or farmer. 

## Invite a New Member to FarmOS

To invite a new member, select the drop-down menu titled Members with Access to Farm, then `Invite Member to Organization`. This will open a new window where you can input your farmer’s email. 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys10.png)


New window: 
![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys11.png)

Once you’ve input your farmer’s email and hit submit, when you return to the original screen you will see a Dialog message: 

![Surveys Tutorial](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/surveys12.png)

Press `Refresh Members`, and your newly-invited farmer’s email will appear in the Members with Access to Farm box. Select it and you can finish registering the FarmOS instance, and your partner will receive their invitation. 

We’re very excited about the improved functionality of this process! If you have any questions or recommendations for further improvements, please let us know. 

