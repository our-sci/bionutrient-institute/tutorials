# Partnerships

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

## BFA - Bionutrient Food Association

[Website](https://bionutrient.org/site/)


## NOFA-Mass - Northeast Organic Farming Association

[Website](https://www.nofamass.org/)


## PASA Sustainable Agriculture

[Website](https://pasafarming.org/)


## Pipeline Foods

[Website](https://www.pipelinefoods.com/)


## Past Partners 

### OAK - Organic Association of Kentucky 

[Website](https://www.oak-ky.org/)


### ROP - Real Organic Project

[Website](https://www.realorganicproject.org/)


### VABF - Virginia Association of Biological Farming

[Website](https://vabf.org/)
