# How to Collect a Sample

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

**Watch a video tutorial here: [RFC Sample Tutorial](https://www.youtube.com/watch?v=stivW-pIcE0)**
 

<iframe width="560" height="315" src="https://www.youtube.com/embed/stivW-pIcE0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Note: We've updated several steps in our process since creating this video. Please read on for clarification!

## Preparation

Additional data and information needed to collect a sample are provided in SurveyStack. Make sure that you choose the appropriate survey for [where you’re at in the process.](https://our-sci.gitlab.io/bionutrient-institute/tutorials/grower_partner_surveys/)

 In your browser or in the SurveyStack app,  open the [Sample Collection Survey - Community Partners](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1) while you have an internet connection.

 - You will need to submit a new Sample Collection Survey for each different crop.
 - You will need this information to complete the survey:
    - FarmOS *Planting*, created when you filled out the **[Planting survey](https://bionutrient.surveystack.io/surveys/611aceeb7a10b50001a86ce3)** - for 2021, you can now draw these directly in your SurveyStack survey! 
    - Crop variety

## Sample Best Practices 

Collection quality is at the core of our research, and has the potential to influence our data significantly. While we recognize that these guidelines cannot be followed all the time in all cases, we need your help as data partners to achieve them as often as possible, and to get samples to our lab facilities in the best possible condition for analysis.

### Critical Requirements:
 - Bag soil and food samples within an hour of being collected.
- Mail the samples as soon as possible, ideally right after collection. The best days to sample are Monday, Tuesday, and Wednesday, so that samples will arrive before the weekend, and won't sit in a mailbox or post office and rot.
- If at all possible, *chill samples prior to mailing.*  This is particularly important for leafy greens, which need to be maintained at cool temperatures to make it to the lab in a useable state. 
- Do not send rotting samples, or samples that are already heavily damaged. For soft fruit, select the firmest samples you have avialable as they will ripen in transit. 

### Important Guidelines: 
- Randomly choose your samples (literally close your eyes and point to one!). Only discard it if it is rotten or not saleable (far too small, broken / cracked, etc.). This helps us receive non-biased, representative individuals. 
- Stick to your sampling plan, or communicate with us if you need to change it.
- Send as many samples as possible in the same box. 

### Supplies

Each sampling box will contain the following: 

- 1 – soil probe, with etch marks at 4” and 8”

-  3 – sample packets: Pre-labeled with the postage already paid

![Photo of sampling packets with instructions, measuring cup and soil probe](https://gitlab.com/our-sci/resources/-/raw/master/images/samples_image.png)

In each sample packet:

- Appropriate sampling container for your crop plan--This includes EITHER:
    - Zip-top bag - for sturdy crops,
    - Foil bag with paper towel liner - for leafy greens, 
    - Zip-top bag with foam sheet - for large fruit OR
    - Produce cup  - for small fruit.
 
- 1 – 0-4” soil sample bag
-  1 – 4-8” soil sample bag

### Sample Survey

Before heading to the field:

 - In your phone's browser or the RFC SurveyStack App, open the [Sample Collection Survey](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1) while you have internet connection.

If you can see the survey in the app, then you will be able to start new surveys in the field, even without internet connection. This will allow you to save surveys (offline) and then submit the surveys once you get back to having an internet connection. Once you have internet connection again, navigate to the `Surveys` screen from the app menu and select the `Active` tab from the top of the page. Then select the surveys that you filled out and hit `SEND`.

Once you’re in the Sample Collection Survey form, the instructions in the survey will guide you through the process.

Make sure to collect the GPS coordinates; these will be kept private. You only need to collect the GPS coordinates for each batch of three samples (same crop type and variety).


![<img src="https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/collect_gps.png">](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/collect_gps.png "Screenshots of GPS location finding in SurveyStackApp" )

## Collecting Samples

- A sample = 1 composite vegetable sample + 1 soil sample (0-4 inches) + 1 soil sample (4-8 inches) near where that vegetable was grown (within 12-18 inches).
- Place each sample in its own sample bag and enter the ID into the survey. Then collect the soil sample associated with it (see next section).

- How to treat the samples:
    1. Do NOT wash the samples. If they are damp, dry them before packing.
    2. For leafy greens, wrap the sample in the enclosed paper towel. This will absorb moisture released in transit, and reduces the incidence of rot.  
    3. For peachs, pears, and nectarines, wrap each individual in one of the foam sheets provided. 
    3. For food samples, do not remove all of the air from the bag before sealing it. It is better to leave some ambient air in the bag to reduce damage/rot during shipping.

NOTE: If you collected samples for us in 2020, you may have differentiated your collection between ‘Individual’ and ‘Composite’ samples. In order to streamline our processes (and make things easier for our partners) we are moving to ONLY composite samples for 2021. For this reason, we also ask that you do not reuse sampling supplies from last year to avoid confusion. 

### What is a 'Composite' Sample?

- Sturdy crops - Beet, Carrot, Potato, Sweet Pepper, Squash or Zucchini: Three individuals.
- Leafy Greens - Bok Choy, Head lettuce, Kale, Leeks, Spinach, Swiss chard, Mustard, Mizuna:  – A large bunch, three small individuals, or three heads.
- Large fruit - Apple, Peach, Pear, Nectarine - Three individuals
- Small fruit - Blueberries, Grapes, Tomatoes: - One full fruit cup. 
- Grains - Wheat, Oats: One full quart bag.

![Nectarines prepped for shipping](https://gitlab.com/our-sci/resources/-/raw/3f6be9e868ea071cc673f2d48c81e5cf429c0e60/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(4).jpg)

*Preparing nectarines for shipping.*

![Nectarines wrapped for shipping](https://gitlab.com/our-sci/resources/-/raw/05e150045eba0d3445026637cb2fa2ea68e7b58d/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(8).jpg)

### Soil Sampling 

Collect a soil sample from within 12-18 inches of EACH produce sample.

 - Using the etch marks on the soil sampler, sample down to 8 inches and put the 0-4 and 4-8 inch samples in the appropriately labeled bags. 
 - After collecting soil cores from around each of your sample plants, you should have enough soil to fill or nearly fill the quart bags provided. If the bags are not full, take additional cores to ensure the lab has enough soil for all processes. 
 - For soil samples, you should remove all of the air from the sample bag before sealing the bags.
 - It is critical that the numbers on the soil sample bags match the number on the produce sample bag. See figure below.

![Image of soil probe and soil horizons](https://gitlab.com/our-sci/resources/-/raw/master/images/soilsample.png)


**One complete sturdy vegetable sample:**
![Image of sturdy veg and soil bags](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/Sample%20kit/20210723_160621.jpg)

**One complete leafy green sample:**
![Image of leafy greens and soil bags](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/Sample%20kit/20210723_160739.jpg)

## Shipping Samples
Once you have your produce and soil samples in hand, lay out all your supplies: 

- Padded envelopes
- Labels
- Sample bags
- Information for survey: management, location, number of samples, name and number of treatment, associated FarmOS data if applicable

Fill out the information in the survey, and place each composite produce sample and two associated soil samples (with matching label numbers) into the padded envelope, keeping the following in mind: 

1. Minimize physical damage - try not to crush your sample! Choose samples that will fit in the bag provided. The bottoms of the foil envelopes for greens fold out, so it can help to make sure they're fully open before putting your greens inside.
2. Include the appropriate amount of air - NONE in soil samples, SOME in produce samples. 
3. Work to minimize damage during transit - plan to mail as soon as possible after sampling, mail early in week, and keep at cool temperatures and/or refrigerate until you can get the samples to a post facility.

Remember: the better condition the sample arrives at the lab in, the better the information will be that we’re able to assess and deliver back to you.


## Checklist
☐ Complete Planting form

☐ Follow directions in Sampling form

☐ Collect the produce sample that matches sampling plan

☐ Collect a 0”-4” and 4”-8” soil sample from within a foot of produce sample

☐ Check that the numbers on produce and soil samples match each other, and match what was entered in the survey

☐ Seal all three samples together in the prepaid sampling envelope

☐ Mail sample envelopes through USPS

☐ Make sure Sampling form is submitted.

☐ Congratulate yourself on contributing to scientific research, and look forward to receiving your results in 3-6 weeks



