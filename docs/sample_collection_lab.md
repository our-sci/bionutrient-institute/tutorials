# **How to Collect a Sample**

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

![Vegetable Garden](https://upload.wikimedia.org/wikipedia/commons/3/3c/Vegetable_garden_1.JPG "Image of a vibrant vegetable garden")

# Please read before you begin! 


## Surveys

You will need to submit each survey _**once per sample**_. Ex: sending in five samples, you'll need 5 planting, 5 sample collection, and 5 harvest forms completed. 

- **Planting Survey** - Coming soon. You will receive a worksheet via email, please use this to record your information until this survey is ready.  
  - If you're submitting soil records, you will need to document planting information ahead of time and attach your sample number to your planting record. You can do this either by making notes on the worksheets your project partner provided, or by completing a Planting Survey.  
- **[Sample Collection Survey](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1)** - Complete this survey while you collect your sample.  
- **Harvest Survey** - Coming soon. You will receive a worksheet via email, please use this to record your information until this survey is ready.  
- **CoffeeShop Enrollment Survey** - Coming soon. This survey will act as a sign up for the Digital CoffeeShop.  

 If you'd like to use these surveys offline, first open them in your browser or in the SurveyStack app while you have internet connection.  


## Sample Best Practices 

Collection quality is at the core of our research, and has the potential to influence our data significantly. While we recognize that these guidelines cannot be followed all the time in all cases, we need your help as data partners to achieve them as often as possible, and to get samples to our lab facilities in the best possible condition for analysis.

- Bag soil and food samples within an hour of being collected.
- Mail the samples as soon as possible, ideally right after collection. The best days to sample are Monday, Tuesday, and Wednesday, so that samples will arrive before the weekend, and won't sit in a mailbox or post office and rot.
- If at all possible, *chill samples prior to mailing.*  This is particularly important for leafy greens, which need to be maintained at cool temperatures to make it to the lab in a useable state. 
- Do not send rotting samples or samples that are already heavily damaged. For soft fruit: select the firmest samples you have available, as they will ripen in transit.

### Supplies:

Your sampling supplies will look something like this: 


![Photo of sampling packets with instructions, measuring cup and soil probe](https://gitlab.com/our-sci/resources/-/raw/master/images/samples_image.png)

    - Postage-paid shipping envelopes
    - Appropriate sampling containers for your crop plan. This includes:
        - Zip-top bag - for sturdy crops,
        - Foil bag with paper towel liner - for leafy greens, 
        - Zip-top bag with foam sheet - for large fruit 
        - Produce cup  - for small fruit.
    
    -  0-4” & 4-8” soil sample bags, if applicable. 
    - Soil probe (if needed for $40)  


## Sample Collection Steps

Before collecting your samples, please complete a Planting Survey (since the survey is under construction, use the planting worksheet) for every sample you plan to send.  

### Step 1: Collect Crop
A complete Linked Crop + Soil sample will contain your *composite vegetable sample, plus a 0-4" soil sample and a 4-8" soil sample* from near where that vegetable was grown (within 12-18 inches).  
A complete Crop Only sample will contain only your *composite vegetable sample*, no soil.  

1) Lay out the sampling materials sent from the Bionutrient Institute.  
2) Notice the Sample ID # located on the sampling bags, write this number down along with the crop you plan to pair with it.  
3) Randomly choose the pieces that will comprise your sample (literally close your eyes and point to one!). Only discard it if it is rotten or not saleable (far too small, broken / cracked, etc.). This helps us receive non-biased, representative individuals.  
    - You will be collecting **composite samples**. See the below list for what makes up a composite sample.
      - Sturdy crops - Beet, Carrot, Potato, Sweet Pepper, Squash or Zucchini: Three individuals
      - Leafy Greens - Bok Choy, Head lettuce, Kale, Leeks, Spinach, Swiss chard, Mustard, Mizuna:  – A large bunch, three small individuals, or three heads
      - Large fruit - Apple, Peach, Pear, Nectarine, Tomato - Three individuals
      - Small fruit - Blueberries, Grapes, Tomatoes: - One full fruit cup 
      - Grains - Wheat, Oats: One full quart bag

![Nectarines prepped for shipping](https://gitlab.com/our-sci/resources/-/raw/3f6be9e868ea071cc673f2d48c81e5cf429c0e60/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(4).jpg)

*Preparing nectarines for shipping.*

![Nectarines wrapped for shipping](https://gitlab.com/our-sci/resources/-/raw/05e150045eba0d3445026637cb2fa2ea68e7b58d/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(8).jpg)

4) Prep your sample.  
    - Do NOT wash the samples. If they are damp, dry them before packing.  
    - For leafy greens, wrap the sample in the enclosed paper towel. This will absorb moisture released in transit, and reduces the incidence of rot.  
    - For peachs, pears, and nectarines, wrap each individual in one of the foam sheets provided. 
    - For food samples, do not remove all of the air from the bag before sealing it. It is better to leave some ambient air in the bag to reduce damage/rot during shipping.
5) Place each sample in its own sample bag and enter the ID into the survey. Then collect the soil sample associated with it (see next section).


### Step 2: Collect Soil (skip to step 11 if you're collecting a Crop Only sample)

6) Find the soil sample bags that have a matching Sample ID # to that of your crop.  
7) Collect a soil sample from within 12-18 inches of EACH produce sample.  
8) Using the etch marks on the soil probe, sample down to 8 inches and put the 0-4 and 4-8 inch samples in the appropriately labeled bags. 
   - After collecting soil cores from around each of your sample plants, you should have enough soil to fill or nearly fill the quart bags provided. If the bags are not full, take additional cores to ensure the lab has enough soil for all processes. 
9) For soil samples, you should remove all of the air from the sample bag before sealing the bags.
10) Remember - It is critical that the numbers on the soil sample bags match the number on the produce sample bag. See figure below.

![Image of soil probe and soil horizons](https://gitlab.com/our-sci/resources/-/raw/master/images/soilsample.png)


**One complete sturdy vegetable sample:**
![Image of sturdy veg and soil bags](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/Sample%20kit/20210723_160621.jpg)

**One complete leafy green sample:**
![Image of leafy greens and soil bags](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/Sample%20kit/20210723_160739.jpg)


### Step 3: Ship Samples
11) Once you have your filled sample bags in hand, lay out all your supplies: 

    - Padded envelopes
    - Labels
    - Sample bags
    - Any records or labeling information for your surveys

12) Complete the [Sample Collection Survey](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1) once per sample. Fill out the information in the survey, and hit "Submit".  
13) Place each composite sample into the padded envelope, keeping the following in mind: 

    - Minimize physical damage - try not to crush your sample! Choose samples that will fit in the bag provided. The bottoms of the foil envelopes for greens fold out, so it can help to make sure they're fully open before putting your greens inside.
    - Send as many samples as possible in the same box or bag. 
    - Include the appropriate amount of air - NONE in soil samples, SOME in produce samples. 
    - Work to minimize damage during transit - plan to mail as soon as possible after sampling, mail early in week, and keep at cool temperatures and/or refrigerate until you can get the samples to a post facility.

Remember: the better condition the sample arrives at the lab in, the better the information will be that we’re able to assess and deliver back to you.
You can look forward to your results in 3-6 weeks! Thanks for sampling with the Bionutrient Institute.  


## Overview Checklists

### Checklist for Crop & Soil Sampling
☐ Complete the Planting survey (or planting worksheet)  
☐ Collect the produce sample that matches your sampling plan  
☐ Collect a 0”-4” and 4”-8” soil sample from within a foot of produce sample  
☐ Check that the Sample ID numbers on produce, soil sample bags, and Sample Collection survey match!  
☐ Seal all three sample bags together in the prepaid sampling envelope  
☐ Mail sample envelopes through USPS  
☐ Make sure the Sample Collection survey is submitted  
☐ Complete the Harvest Survey (or harvest worksheet)  
☐ Congratulate yourself on contributing to scientific research, and look forward to receiving your results in 3-6 weeks!  


### Checklist for Crop Only Sampling
☐ Complete the Planting survey (or planting worksheet)  
☐ Collect the produce sample that matches your sampling plan  
☐ Check that the Sample ID numbers on produce bags and Sample Collection survey match!  
☐ Seal your samples in the prepaid sampling envelope - send as many as possible in the same package  
☐ Mail sample envelopes through USPS  
☐ Make sure the Sample Collection survey is submitted  
☐ Complete the Harvest Survey (or harvest worksheet)  
☐ Congratulate yourself on contributing to scientific research, and look forward to receiving your results in 3-6 weeks!  

