# How to Send a Sample 

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

Here's a video tutorial walking you through the sampling process: [Citizen Science Sample Collection Tutorial](https://www.youtube.com/watch?v=Frz_D4p4oEI)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Frz_D4p4oEI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Preparation
After we receive your onboarding information, we'll send you a sample collection kit containing the following: 

* Your personalized sampling schedule
* Numbered sampling bags
* Postage-paid shipping envelopes


In your browser or in the SurveyStack app, open the [Sample Collection Survey - Community Partners](https://bionutrient.surveystack.io/surveys/60b69a34e4f24b0001f6f6f1) while you have an internet connection.

If you need additional support to navigate the app, see our full tutorial here: [SurveyStack - Navigating the App](https://our-sci.gitlab.io/software/surveystack_tutorials/navigation/)

## What goes in a sample kit?
Each sample kit consists of two paired sample bags for the crop you'd like to compare, with three individuals in each bag. For example, a sample kit could be: 

  * Three organic nectarines and three conventional nectarines 
  * Three farmer's market potatoes and three grocery store potatoes
  * A quart bag with low-till wheat, and a quart bag with high-tillage wheat


By mixing the three individuals in each sample in the BI lab, we can get a better idea of the average nutrient density of these samples. 


## Sample Best Practices

* Mail the samples as soon as possible, ideally right after collection. The best days to sample are Monday, Tuesday, and Wednesday, so that samples will arrive before the weekend, and won't sit in a mailbox or post office and rot.

* Keep a personal record of the sample numbers (found on the zip-top bags) & which of your samples correspond to each sample number.

![sample number](https://gitlab.com/our-sci/resources/-/raw/e691a8a11f2037769c24efc7b8bc6624048200cf/images/Partner%20tutorials/sample%20number.png)

* Often produce is already packaged to preserve freshness, so keeping produce in its original packaging means we get fresher samples in the lab. 

* Do not send rotting samples, or samples that are already heavily damaged. When selecting fruit samples, choose the firmest fruits possible, as they will ripen in transit. 

![Nectarines prepped for shipping](https://gitlab.com/our-sci/resources/-/raw/3f6be9e868ea071cc673f2d48c81e5cf429c0e60/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(4).jpg)

*Preparing nectarines for shipping.*

![Nectarines wrapped for shipping](https://gitlab.com/our-sci/resources/-/raw/05e150045eba0d3445026637cb2fa2ea68e7b58d/images/Partner%20tutorials/Sample%20kit/soft%20fruit%20shipping(8).jpg)
### Important Guidelines:
* Fill out the Sample Collection survey at the same time that you pack and mail your samples to make sure we have all the information that we need.
* Stick to your sampling plan, or communicate with us if you need to change it.
* Send as many samples as possible in the same package.

## Shipping

Once you have your samples in hand, lay out all your supplies: 

- Padded envelopes 
- Labels 
- Sample bags 
- Information for survey: management, location, marketing

Fill out the information in the survey, and place each produce sample into the padded envelope, keeping the following in mind:

* Minimize physical damage - try not to crush your sample! Choose samples that will fit in the bag provided. 
* Leave some air in produce sample bags, this helps preserve freshness.
* Work to minimize damage during transit - plan to mail as soon as possible after sampling, mail early in week, and keep at cool temperatures and/or refrigerate until you can get the samples to a post facility.
* Remember: the better condition the sample arrives at the lab in, the better the information will be that we’re able to assess and deliver back to you.

## Checklist 

☐ Follow directions in Sampling form

☐ Collect the produce sample that matches sampling plan

☐ Seal samples in prepaid sampling envelope

☐ Mail sample envelopes through USPS

☐ Make sure Sampling form is submitted.

☐ Congratulate yourself on contributing to scientific research!
