# Viewing Data

<!-- Global site tag (gtag.js) - Google Analytics --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> 
<script> 
  window.dataLayer = window.dataLayer || []; 
  function gtag(){window.dataLayer.push(arguments);} 
  gtag('js', new Date()); 
 
  gtag('config', 'G-ZLMNWKMFSL'); 
</script> 

## Data Exploration

### Data Exploration Dashboard

This is one way we've been working to help you visualize your data in comparison with other farms in other regions-

 [Data Exploration Dashboard](https://app.surveystack.io/static/pages/dataExplorer)

![Data Explorer - Logged In](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/dataexplorer/datadashoverview.png)


## Viewing Your Results
Anyone who submits samples to the BI lab, either from their farm or from farmers markets or grocery stores, can view their results in the **Data Explorer**. First, log in to the Data Explorer using your SurveyStack login. Then, you can view your results by either selecting **My Data** from the **Visualization Mode** or by selecting **Download My Data**.

Lab results are linked to the account that submitted the **Sample Collection Form.** If you do not see your results in the **My Data** that most likely means either:

1. The email you used to log in to the Data Explorer was not the same email used to submit the samples OF
2. The analysis is not complete yet.

 *Remember, if some data is missing, the laboratory analysis may not be completed yet. This link will automatically update when new results come in.*

 Contact partners@bionutrient.org if you have any questions about your results.

![Data Explorer - Close Up](https://gitlab.com/our-sci/resources/-/raw/master/images/Partner%20tutorials/dataexplorer/datadash3.png)

<br>

### Interpreting Your Results
#### Crop samples
- For each crop sample, the nutrient value and its ranking within the BI database are displayed
- Produce rankings are separated by crop.
- The rankings are from lowest to highest, so the 100% percentile is the highest value for that parameter.
- In the table below, the antioxidant content for the 1st sample was 1453 mg / 100 g fresh weight, which was in the 22% percentile of all samples of that crop. 

![Presentation1.jpg](./Presentation1.jpg)


#### Crop parameters
*all crop values are adjusted for moisture content and normalized*

| Header | Description | Units | 
| ------------- | -------------- |--------------- |
|Brix | Brix | Bx |
| Antioxidants | Antioxidant content, total | FRAP units per 100 g Fresh weight|
| Polyphenols | Polyphenol content, total | mg GAE per 100 g Fresh Weight |
| Crop, P | Phosphorus content, total | mg per 100 g fresh weight  |
| Crop, S | Sulpher content, total | mg per 100 g fresh weight  |
| Crop, K | Potassium content, total | mg per 100 g fresh weight  |
| Crop, Ca | Calcium content, total | mg per 100 g fresh weight  |
| Crop, Mg | Magnesium content, total | mg per 100 g fresh weight |


#### Soil samples - BI Lab
At the BI lab, soil samples are analyzed for organic matter, total organic carbon and soil respiration at 0-4 and 4-8 inch depth increments.

*In this section, the percentile ranking are from all soils in the BI database, regardless of the crop planted.*

| Header | Description | Units | 
| ------------- | -------------- |--------------- |
| Soil 0-10cm Organic Matter % | Organic Matter, 0-4 inches| % |
| Soil 0-10cm Total Organic Carbon % | Total Organic Carbon, 0-4 inches | %|
| Soil 0-10cm pH | pH, 0-4 inches | no units |
| Soil Respiration 0-10cm | Soil Respiration, 0-4 inches. Total amount of carbon respired by soil microorganisms in a 24 hour period.  | ug C / g soil | 
| Soil 10-20cm Organic Matter % | Organic Matter, 4-8 inches| % |
| Soil 10-20cm Total Organic Carbon % | Total Organic Carbon, 4-8 inches | %|
| Soil 10-20cm pH | pH, 4-8 inches | no units |
| Soil Respiration 10-20cm | Soil Respiration, 4-8 inches. Total amount of carbon respired by soil microorganisms in a 24 hour period.  | ug C / g soil | 


#### Soil samples - Logan Lab
Using the remaining soil, subsamples from each depth were mixed together to form a single 0-8 inch sample that was sent to [Logan Labs](https://www.loganlabs.com/) for analysis.

Soil parameters measured at Logan labs and their units

| Header | Description | Units | 
| ------------- | -------------- |--------------- |
| Soil Total Exchange Capacity | Soil Total Exchange Capacity| meq/100g |
| Soil Ph | pH| no units |
| Soil Om Percent | Organic Matter| % |
| Soil S Available Ppm | Sulpher, available| ppm |
| Soil P ppm | Phosphorus| ppm |
| Soil Ca Desired ppm | Calcium, desired | ppm |
| Soil Ca ppm | Calcium, actual | ppm |
| Soil Ca Deficit ppm | Calcium, deficit | ppm |
| Soil Mg Desired ppm | Magnesium, desired | ppm |
| Soil Mg ppm | Magnesium, actual | ppm |
| Soil Mg Deficit ppm | Magnesium, deficit | ppm |
| Soil K Desired ppm | Potassium, desired | ppm |
| Soil K ppm | Potassium, actual | ppm |
| Soil K Deficit ppm | Potassium, deficit | ppm |
| Soil Na ppm | Sodium | ppm |
| Soil Ca Base Percent | Calcium, base percent | % |
| Soil Mg Base Percent | Magnesium, base percent | % |
| Soil K Base Percent | Potassium, base percent | % |
| Soil Na Base Percent | Sodium, base percent | % |
| Soil Other Base Base Percent | Other bases, base percent | % |
| Soil H Base Percent | Hydrogen, base percent | % |
| Soil B Available ppm | Boron, available | ppm |
| Soil Fe Available ppm | Iron, available | ppm |
| Soil Mn Available ppm | Manganese, available | ppm |
| Soil Cu Available ppm | Copper, available | ppm |
| Soil Zn Available ppm | Zinc, available | ppm |
| Soil Al Available ppm | Aluminum, available | ppm |
| Soil Co Available ppm | Cobalt, available | ppm |
| Soil Mo Available ppm | Molybdenum, available | ppm |
| Soil Se Available ppm | Selenium, available | ppm |
| Soil Si Available ppm | Silicon, available | ppm |
| Soil N Release Acre | Estimated N release | lbs/Acre|
| Soil Electrical Conductivity | Electrical Conductivity | mmhos/cm |

## Dashboard FAQ

* **How does it work?**
 
    We've collected all the field data from our [2020](https://lab.realfoodcampaign.org/survey-2020/) reports and put it into one place, so you can visually compare practices. By changing the factor you're interested in using the gold buttons at the top of the screen, you can see different factors side-by-side. You can choose `Crop` or `Source` in the lower right hand, and by switching the Visualization Mode from `Synthesis` to `Detailed`, you can see the range covered in our sample set. 

<br>


* **What is BQI?**

    To simplify interpretation of complex nutrient density data, we developed an index value that we refer to as the Bionutrient Quality Index (BQI) to aggregate multiple quality measurements into a single value. These include: 

 - Antioxidants
 - Polyphenols
 - Brix (for vegetable crops) or Protein (for grain crops)
 - Five minerals important for human health: magnesium, sulphur, potassium, calcium, and phosphorus.

    This index value is our starting point for developing a practical and measurable definition for nutrient density in crops and does not represent scientific consensus, but is a useful placeholder that we can develop a framework around for today.  

<br>

* **How do I sort by location? What are "Climate Regions"?**

    We use the NOAA Climate Regions map to delineate geographic areas for comparison: 

    ![Climate Map](https://www.ncdc.noaa.gov/monitoring-content/monitoring-references/maps/images/us-climate-regions.gif "NOAA Climate Regions")

    To filter by region in the data explorer, scroll down to the Climate Region section and select the region you're interested in. 

    ![Climate Regions Filter](https://gitlab.com/our-sci/resources/-/raw/a0894a21615afbb1bfb277339026e714bcefe53f/images/Partner%20tutorials/dataexplorer/climate%20regions1.png)

    Then select "Add Filter".
<br>


### How is the data generated by this project used?
1. Calibrating the handheld Bionutrient Meter, which will eventually be used by consumers to non-destructively predict the nutritional density of food.
2. Generating an independent, geographically diverse public database of food quality across the supply chain, complete with crop management information, to better understand what drives nutritional density in food.

## Data Privacy Policy
This section relates to data collected as part of the Real Food Campaign & Bionutrient Institute Food/Soil surveys.

**We believe strongly that:**

1. Farms that collect data have the right to keep their personal information private, and
2. Sharing information is critical to building high-quality, trusted, and effective tools for measuring food quality.

**Data rights and use**

Sample data collected as part of the survey will be made public. If collected from a grocery store, all data will be public. If samples are collected from a farmers market or farm, the producer’s personal data will be anonymized by default. This includes their farm name, farm address, email, GPS location, and related identifying information. For these anonymized samples, the only location information made public is the county and state – meaning the public will know that a farm in this county had a carrot which had these results, but not know who or where exactly.

When collecting samples from a farmer or at farmers’ market, farmers may include their email so that we can email them their results. This will come as an email with a unique link they can use to access their results. Others can only access these de-anonymized results if that link is shared to them, so keep your unique link private to protect your data.

By submitting samples to the Bionutrient Institute, those submitting samples consent to ongoing use and distribution of the anonymized data (metadata and lab data, as described above) from that sample and its environment.

**Who else can access the de-anonymized data and why?**

Maintainers of the Our Sci data management platform may access the data for purposes of necessary fixes or improvements the software or database.
Bionutrient Institute management may access the data for statistical analysis. No outputs of this analysis will be made public with normally anonymized data visible (farm name, email, location, GPS), and no de-anonymized data will ever be shared with any third parties for any reason.
